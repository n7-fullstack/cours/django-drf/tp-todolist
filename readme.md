# TP Django / DRF Todo list

## Installation

Create virtualenv

```
python3 -m venv <virtualenv_name>
```

Install dependencies

```
pip install -r requirements.txt
```

Run migrations

```
cd todoproject
./manage.py migrate
```

Run server

```
./manage.py runserver
```

