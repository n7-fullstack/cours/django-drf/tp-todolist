from django import forms
from todolist.models import Task


class TaskForm(forms.ModelForm):
    """Task form"""

    class Meta:
        model = Task
        # fields = ['title', 'deadline', 'done']
        fields = '__all__'
