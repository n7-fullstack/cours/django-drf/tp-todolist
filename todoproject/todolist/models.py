from django.db import models


class TaskList(models.Model):
    """Model for task list"""

    name = models.CharField(verbose_name="Name", max_length=255)

    def __str__(self) -> str:
        return self.name


class Task(models.Model):
    """Model for task"""

    title = models.CharField(verbose_name="Title", max_length=255)
    task_list = models.ForeignKey(
        TaskList, null=True, blank=True,
        on_delete=models.CASCADE,
        related_name="tasks"
    )
    deadline = models.DateField(null=True, blank=True)
    done = models.BooleanField(default=False)

    def __str__(self) -> str:
        return f"{self.title}"
