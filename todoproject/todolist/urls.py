from django.urls import path, include
from todolist import views

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'tasks', views.TaskViewSet)

urlpatterns = [
    path('about/', views.AboutView.as_view()),
    path('archives/<int:year>/<int:month>/', views.ArchiveView.as_view(), name="archive"),
    path('tasks/', views.TasksView.as_view()),
    path('tasks/add', views.AddTaskView.as_view(), name="add_task"),
    path('tasks/<int:pk>', views.TaskDetailView.as_view(), name="task_detail"),
    path('api/v1/', include(router.urls)),
    path('api/v0/tasks', views.APITaskView.as_view(), name="api_tasks_list"),
    path('api/v0/tasks/<int:id>', views.APITaskDetailView.as_view()),
]
