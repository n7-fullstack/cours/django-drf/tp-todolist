from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic import (
    View, TemplateView,
    ListView, DetailView, CreateView
)
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from todolist.models import Task
from todolist.forms import TaskForm
from todolist.serializers import TaskSerializer, SimpleTaskSerializer


def index(request):
    return HttpResponse("<h1>Bienvenue sur notre application</h1>")

class TasksView(ListView):
    model = Task


class TaskDetailView(DetailView):
    template_name = "todolist/task_detail.html"
    model = Task


class AddTaskView(CreateView):
    form_class = TaskForm
    success_url = '/todo/tasks/'  # reverse_lazy('task:list')
    template_name = 'todolist/add_task.html'


def add_task(request):
    form = TaskForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/todo/tasks/')
    return render(request, 'todolist/add_task.html', {'form': form})

class IndexView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse("<h1>Bienvenue mais en class-based view</h1>")


class AboutView(TemplateView):
    template_name = "about.html"


class ArchiveView(View):
    def get(self, request, year, month):
        if year > 2023:
            return HttpResponse("sélectionnez une ancienne année")
        return HttpResponse(f"{month}, {year}")


class TaskViewSet(viewsets.ModelViewSet):
    """Viewset for Task"""

    queryset = Task.objects.all().order_by("title")
    serializer_class = TaskSerializer


class APITaskView(APIView):
    """API View for tasks"""

    def get(self, request, format=None):
        tasks = Task.objects.all()
        serializer = SimpleTaskSerializer(tasks, many=True)
        response_data = {'results': serializer.data}
        return Response(response_data)

    def post(self, request, format=None):
        serializer = SimpleTaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class APITaskDetailView(APIView):
    """API View for task detail"""

    def get(self, request, id, format=None):
        task = Task.objects.get(id=id)
        serializer = SimpleTaskSerializer(task)
        response_data = {'results': serializer.data}
        return Response(response_data)

    def delete(self, request, id, format=None):
        try:
            task = Task.objects.get(id=id)
            task.delete()
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def patch(self, request, id, format=None):
        try:
            task = Task.objects.get(id=id)
            serializer = TaskSerializer(task, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        