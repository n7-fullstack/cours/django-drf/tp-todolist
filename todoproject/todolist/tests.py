from django.test import TestCase
from todolist.models import Task


class TaskTest(TestCase):
    """Test sur les tâches"""

    def setUp(self):
        self.task1 = Task.objects.create(title="Ma tâche")

    def test_str(self):
        self.assertEqual(self.task1.__str__(), "Ma tâche")

    def test_task_list(self):
        response = self.client.get('/todo/tasks/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Ma tâche")

    def test_task_add(self):
        self.client.post(
            '/todo/tasks/add',
            data={'title': 'Faire le test'}
        )
        tasks = Task.objects.filter(title='Faire le test')
        self.assertEqual(tasks.count(), 1)
