from rest_framework import serializers

from todolist.models import Task, TaskList


class TaskListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TaskList
        fields = ('name', 'task_set')


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    task_list = serializers.StringRelatedField()
    
    class Meta:
        model = Task
        fields = ('title', 'deadline', 'done', 'task_list')
        # fields = '__all__'


class SimpleTaskSerializer(serializers.Serializer):
    """Simple serializer for Task"""
    id = serializers.IntegerField(required=False)
    title = serializers.CharField()
    deadline = serializers.DateField(required=False)
    done = serializers.BooleanField()
    task_list = serializers.SlugRelatedField(read_only=True, slug_field='name')

    def create(self, validated_data):
        return Task.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.deadline = validated_data.get('deadline', instance.deadline)
        instance.done = validated_data.get('done', instance.done)
        instance.save()
        return instance